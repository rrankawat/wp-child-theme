<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-child-theme' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RZ.,vF+cdc=hWhZ[CU-V2A5q$wXKp3Sh`s8/Gv#_e7x@9l5eraFbCmOk.}Q(=m>~' );
define( 'SECURE_AUTH_KEY',  '-*^v}2ltpnv+KBGH*Rz|iA[Bs*|d([zEIx8VE9.LC&fIM1]+i(0[:Ke=T?Ya6:yv' );
define( 'LOGGED_IN_KEY',    'ua%FRd~Q]qXCN3C6J$5:)<W4/KliP:H<!!Hj4.Z0Wh=8:5uRxlsplzVT1`Ad 3H*' );
define( 'NONCE_KEY',        ')0ydU|Vmp1M&k?XB5vjoT1;!!V(I:7+:`y:!Kj[hhjR}7PUj3N!9<w-Fni87pb{Y' );
define( 'AUTH_SALT',        '9/yW&iLgE$w^]sJbQ_s U`-yq6[[+2p)nVXu93hjdxN7.v#-?Rh*+](4vv4kGIzO' );
define( 'SECURE_AUTH_SALT', 'T@coMyJ7%i4 |=sQ/OwRjOSli-:dccfP*B#W*X$@a4flB__,L}DBAg,Q]xD(zq%<' );
define( 'LOGGED_IN_SALT',   'q=I_lV93I( @jZScnCa2._pOUP.FV(hZAm}FBT_N[fO/##`qR2)yEU[V8_kjSUt:' );
define( 'NONCE_SALT',       'K@sw<M1|m=Gt$3@6Vhie,94}TP7a=cCvNCy,J=L#GLR;J<=z~AT[/xJAEJ:_Zq`e' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
